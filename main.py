import json
from pprint import pprint
from termcolor import colored

with open('people.json', 'r') as p:
    people = json.loads(p.read())

print(colored("""
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   
""", 'yellow'))
print(colored('Modele des données :', 'yellow'))
pprint(people[0])

# debut de l'exo
print(colored(''.join(['_' for _ in range(80)]), 'green', 'on_green'))

print(colored("Nombre d'hommes : ", 'yellow'))
# pour chaque personne du tableau, si son genre == 'Male' je le mets dans le tableau hommes
hommes = [p for p in people if p['gender'] == 'Male']
# len() revoie la taille (nombre d'élément) d'un tableau
pprint(len(hommes))

################################################################################

# je peux aussi l'écrire avec une boucle classique
hommes2 = []                        # un tableau vide
for person in people:               # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme (2-266-02250-4)
        hommes2.append(person)      # je l'ajoute au tableau
print(len(hommes2))

################################################################################

# dans la même idée, plutot que de mettre tous les hommes dans un tableau
# puis afficher la longueur du tableau, je peux juste les compter dans une variable
nb_hommes = 0                       # je commence à 0
for person in people:               # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme
        nb_hommes = nb_hommes + 1   # j'ajoute 1 à mon compteur
print(nb_hommes)

################################################################################

print(colored("Nombre de femmes : ", 'yellow'))
# je peux compter les femmes ou calculer : nombre d'élement dans people - nombre d'homme

femmes = [p for p in people if p['gender'] == 'Female']
print(len(femmes))
################################################################################

print(colored("Nombre de personnes qui cherchent un homme :", 'yellow'))

choix_hommes = 0
for person in people:
    if person["looking_for"] == "M":
        choix_hommes = choix_hommes + 1
print(choix_hommes)
################################################################################

print(colored("Nombre de personnes qui cherchent une femme :", 'yellow'))

choix_femmes = 0
for person in people:
    if person["looking_for"] == "F":
        choix_femmes = choix_femmes + 1
print(choix_femmes)
######################
################################################################################

print(colored("Nombre de personnes qui gagnent plus de 2000$ :", 'yellow'))

revenu = 0
for person in people:
    if float(person["income"][1:]) > 2000:
       revenu = revenu + 1
print(revenu)
################################################################################

print(colored("Nombre de personnes qui aiment les Drama :", 'yellow'))
# là il va falloir regarder si la chaine de charactères "Drama" se trouve dans "pref_movie"
drama = 0
for person in people:
    if "Drama" in person["pref_movie"]:
        drama = drama + 1
print(drama)

################################################################################

print(colored("Nombre de femmes qui aiment la science-fiction :", 'yellow'))
# si j'ai déjà un tableau avec toutes les femmes, je peux chercher directement dedans ;)
femmes_sf = 0
for person in people:
    if person["gender"] == "Female" and "Sci-Fi" in person["pref_movie"]:
        femmes_sf = femmes_sf + 1
print(femmes_sf)

################################################################################

print(colored('LEVEL 2' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$", 'yellow'))
docus = 0
for person in people:
    if "Documentary" in person["pref_movie"] and float(person["income"][1:]) > 1482:
        docus +=1
print(docus)

################################################################################

print(colored("Liste des noms, prénoms, id et revenus des personnes qui gagnent plus de 4000$", 'yellow'))

for person in people:
    if float(person["income"][1:]) > 4000:
        print(person["id"], person["first_name"], person["last_name"], person["income"])
################################################################################

print(colored("Homme le plus riche (nom et id) :", 'yellow'))

maxi = 0
pers = ""
for person in hommes:   
    if float(person["income"][1:]) > maxi:
        maxi = float(person["income"][1:])
        pers = person
print(pers["id"], pers["last_name"])

################################################################################

print(colored("Salaire moyen :", 'yellow'))

somme = 0
for person in people:
    somme += float(person["income"][1:])
    moyenne = somme / len(people)
print(moyenne)
################################################################################

print(colored("Salaire médian :", 'yellow'))

import numpy as np
liste = []
for person in people:
    liste.append(float(person["income"][1:]))
tableau_sal = np.array(liste)
sal_median = np.median(tableau_sal)
print(sal_median)

################################################################################

print(colored("Nombre de personnes qui habitent dans l'hémisphère nord :", 'yellow'))

hem_n = 0
for person in people:
    if person["latitude"] >= 0:
           hem_n += 1
print(hem_n)
################################################################################

print(colored("Salaire moyen des personnes qui habitent dans l'hémisphère sud :", 'yellow'))

somme = 0
for person in people:
    if person["latitude"] < 0:
        somme += float(person["income"][1:]) 
salaire_moyen = somme / (1000 - hem_n)
print(salaire_moyen)

################################################################################

print(colored('LEVEL 3' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Personne qui habite le plus près de Bérénice Cawt (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
#récupérer id bérénice cawt
berenice = {}
for person in people:
    if person['first_name'] == "Bérénice" and person["last_name"] == "Cawt":
        berenice = person
#print(total_sauf_berenice[0:2])

import math
def distance(person1, person2):
        B = person1["latitude"] - person2["latitude"]
        C = person1["longitude"] - person2["longitude"]
        return math.sqrt(B**2+C**2)

distance_min = 0
le_plus_proche = {}

for person in people:
    if person != berenice:
        dis = distance(berenice, person)
        if distance_min == 0:
            distance_min = dis
        elif dis < distance_min:
            distance_min = dis
            le_plus_proche = person
print(le_plus_proche)
print(le_plus_proche["id"], le_plus_proche["last_name"])


################################################################################

print(colored("Personne qui habite le plus près de Ruì Brach (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))


def recup_plus_proche(prenom, nom):
    ref = {}
    for person in people:
        if person["first_name"] == prenom and person["last_name"] == nom:
            ref = person
    
    distance_min = 0
    le_plus_proche = {}
    for person in people: 
        if person != ref:
            dis = distance(ref, person)
            if distance_min == 0:
                distance_min = dis
            elif dis < distance_min:
                distance_min = dis
                le_plus_proche = person
    return le_plus_proche

le_plus_proche_de_berenice = recup_plus_proche("Bérénice", "Cawt")
print(le_plus_proche_de_berenice["id"], le_plus_proche_de_berenice["last_name"])
le_plus_proche_de_rui = recup_plus_proche("Ruì", "Brach")
print(le_plus_proche_de_rui["id"], le_plus_proche_de_rui["last_name"])
                                               

################################################################################

print(colored("les 10 personnes qui habitent les plus près de Josée Boshard (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

josee = {}
for person in people:
    if person['first_name'] == "Josée" and person["last_name"] == "Boshard":
        josee = person
        print(person["id"])


distances = []
person = []
for person in people:
    if person != josee:
        # distance entre josee et les autres personnes
        dis = distance(josee,person)
        # on récupère toutes les distances entre Josée et les autres membres
        person["distances"] = dis
        distances.append(person)
        # on trie le tableau des distances
        sort_distance = sorted(distances, key = lambda item:item["distances"])

for i in range(1,11):
    print(sort_distance[i]["id"], sort_distance[i]["first_name"], sort_distance[i]["last_name"])



################################################################################

print(colored("Les noms et ids des 23 personnes qui travaillent chez google :", 'yellow'))

for person in people:
    if "google" in person["email"]:
        print(person["id"], person["last_name"])
################################################################################

print(colored("Personne la plus âgée :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
from datetime import datetime


date_of_birth = 0
liste = []
now = datetime.now()
for person in people:
    if now - datetime((person["date_of_birth"])) > date_of_birth:
        liste.append((person["date_of_birth"]))
        
        maximum = max(date_of_birth)
print(person["last_name"], person["id"])


################################################################################

print(colored("Personne la plus jeune :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
print(colored("Moyenne des différences d'âge :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('LEVEL 4' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
print(colored("Genre de film le plus populaire :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Genres de film par ordre de popularité :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des genres de film et nombre de personnes qui les préfèrent :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des hommes qui aiment les films noirs :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des femmes qui aiment les drames et habitent sur le fuseau horaire de Paris : ", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("""Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une
préférence de film en commun (afficher les deux et la distance entre les deux):""", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des couples femmes / hommes qui ont les même préférences de films :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('MATCH' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
"""
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui ont le plus de goûts en commun.
    Puis ceux qui sont les plus proches.
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi 
    les films d'aventure.
    La différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agé des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.                  ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction         ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum     ߷    ߷    ߷    

"""
print(colored("liste de couples à matcher (nom et id pour chaque membre du couple) :", 'yellow'))
print(colored('Exemple :', 'green'))
print(colored('1 Alice A.\t2 Bob B.'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
